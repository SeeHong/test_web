import Vue from "vue";
import Router from "vue-router";
import Landing from "./views/Landing.vue";
import Q2 from "./views/Q2.vue";

Vue.use(Router);

export default new Router({
  routes: [
    {
      path: "/",
      name: "landing",
      component: Landing,
    },
    {
      path: "/q2",
      name: "q2",
      component: Q2,
    },
  ],
});
